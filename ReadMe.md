## Introduction
* This project is under python3.* environment, no other requirement
* This is the classic battle ship game for one company's logic test


## Instruction
* The game is build on a 8 * 8 board, there are two players of the game each player hold two ships.
* Each ship occupied one cell of the board
* The game will be random select two cells for each player at the current version
* Each player input hit coordinates (x,y) at commend line for hit the ship
* For example 3,5, and the computer locates the nearest ship to that co-ordinate. tells them they're "hot" if they're 1 to 2 cells away, "warm" if they're 3 to 4 cells away, or "cold" if they're further away
* Winner is the one who hit both two ships against another
* Deuce will occur when both two player have remainder ship(s) and run out of 20 times tries


## How to run my program
* Just run code_test.py normally, either on editor or on commend line, then follow the instruction
