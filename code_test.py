'''
Environment: python 3.6 no other packages required
'''
import random
random.seed(123)

class Battleship(object):
    '''
    public: board: 8*8 cells contain 2 single cell "ships"
            empty cell token "0", ship cell token "1"
    private: (x1,y1), (x2,y2): coordinates of two ships
    private: hit1,hit2: flags check hit or not
    '''
    def __init__(self):
        self.board = [ [0] * 8 for i in range(8)]
        self._x1, self._y1, self._x2, self._y2 = self.create_two_ships()
        self._hit1, self._hit2 = False,False

    def create_two_ships(self):
        x1, y1= random.randint(0,7),random.randint(0,8)
        x2, y2 = random.randint(0,7),random.randint(0,8)
        self.board[x1][y1] = 1
        self.board[x2][y2] = 1
        return x1, y1, x2, y2

    def draw_battle(self):
        for i in self.board:
            print(i)


    def check_hit(self, input_x, input_y):
        '''
            pre-require: input coordinates not exceed boundary
            check hit for the user enters a co-ordinate,
            for example 3,5, and the computer locates the nearest ship to that co-ordinate.
            tells them they're "hot" if they're 1 to 2 cells away, "warm" if they're 3 to 4 cells away,
            or "cold" if they're further away
        :param input_x:
        :param input_y:
        :return:
        '''
        if input_x < 0 or input_x > 7 or input_y < 0 or input_y >7:
            raise Exception("input out of board range")

        range_with_point1, range_with_point2 = 99,99
        if not self._hit1:
            range_with_point1 = abs(input_x - self._x1 ) + abs(input_y - self._y1)
        if not self._hit2:
            range_with_point2 = abs(input_x - self._x2 ) + abs(input_y - self._y2)

        nearest = min(range_with_point1, range_with_point2)
        if nearest == 0 :
            if nearest == range_with_point1:
                self._hit1 = True
                self.board[self._x1][self._y1] = 0
                return "hit"
            elif nearest == range_with_point2:
                self._hit2 = True
                self.board[self._x2][self._y2] = 0
                return "hit"
        elif nearest >= 1 and nearest <=2:
            return "hot"
        elif nearest >= 3 and nearest <= 4:
            return "warm"
        else:
            return "cold"

    def check_game_finish(self):
        return self._hit1 and self._hit2


    def check_how_many_ship_hit(self):
        # only for print result use
        if not self._hit1 and not self._hit2:
            return 0
        elif self._hit1 or self._hit2:
            return 1


class Game(object):
    '''
    player 1 need human input in text, player 2 will auto run
    '''
    def __init__(self):
        self.player1,self.player2 = Battleship(), Battleship()

    def read_input(self,token):
        ret = input("input %s for guess: "%token)
        while True:
            try:
                ret = int(ret)
                return ret
            except:
                ret = input("Need a intger: input % for guess: "% token)

    def play(self):

        for i in range(20):

            if not self.player1.check_game_finish():
                print("======================================================")
                print("player 1's %i turn: "% i)
                #try_x, try_y= random.randint(0,7), random.randint(0,7)

                try_x = self.read_input("x")

                try_y = self.read_input("y")


                print("player 1 try:  %i, %i" % (try_x,try_y))
                print("result: ", self.player2.check_hit(try_x, try_y))
                if self.player2.check_game_finish():
                    print( "Winner player 1 hit all the ships of player 2 with %i tires" %(i))
                    break

            if not self.player2.check_game_finish():
                print("======================================================")
                print("player 2's %i turn:" % i)
                try_x, try_y = random.randint(0, 7), random.randint(0, 7)
                print("player 2 try:  %i, %i" % (try_x, try_y))
                print("result: ", self.player1.check_hit(try_x, try_y))
                if self.player1.check_game_finish():
                    print( "Winner player 2 hit all the ships of player 1 with %i tires" %(i))
                    break

        print("no winner\nplayer 1 hit %i ship and player2 hit %i ship"
              %(self.player2.check_how_many_ship_hit(), self.player1.check_how_many_ship_hit() ))




if __name__ == '__main__':
    g = Game()
    g.play()
